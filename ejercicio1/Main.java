import  java.io.BufferedReader;
import java.io.IOException;
import  java.io.InputStreamReader;


public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		int opcion = 0;
	    //se crea el objeto persona1 y tarjeta santander
		Persona persona1 = new Persona("Michelle");
		Tarjeta tarjeta_santander = new Tarjeta(persona1, 50000);
		System.out.println("La tarjeta 1 tiene como titular a: " + tarjeta_santander.getTitular().getNombre());
		System.out.println("Y tiene un saldo de: " + tarjeta_santander.getSaldo() + " Pesos");
		
		while(true) {
			
			System.out.println("¿Que desea hacer?");
			System.out.println("1.- Depositar dinero a la tarjeta");
			System.out.println("2.- Retirar dinero de la tarjeta");
			System.out.println("3.- Salir");
			opcion = Integer.parseInt(reader.readLine());
			
			if (opcion == 1) {
				System.out.println("¿Cuanto desea depositar?");
				double compra = Double.parseDouble(reader.readLine());
				tarjeta_santander.realizardeposito(compra);
				System.out.println("Se realizó un deposito a la tarjeta y el nuevo saldo es: " + tarjeta_santander.getSaldo());
			}
			
			else if (opcion == 2) {
				System.out.println("¿Cuanto desea retirar?");
				double compra = Double.parseDouble(reader.readLine());
				tarjeta_santander.realizarCompra(compra);
				System.out.println("Se realizó un retiro a la tarjeta y el nuevo saldo es: " + tarjeta_santander.getSaldo());
			}
			else if (opcion == 3) {
				System.out.println("¡Hasta luego!");
				break;
			}
		
	    }
	}
}
