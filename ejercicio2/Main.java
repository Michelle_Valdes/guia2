import  java.io.BufferedReader;
import  java.io.IOException;
import  java.io.InputStreamReader;
import 	java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        //creacion de objetos de tipo Libro
		Libro libro1 = new Libro("Papelucho", "Marcela Paz", 20, 0);
		Libro libro2 = new Libro("1984", "George Orwell", 10, 2);
		Libro libro3 = new Libro("Brave new world", "Aldous Huxley", 5, 5);
		Libro libro4 = new Libro("Dracula", "Brams Stoker", 4, 15);
		
		Biblioteca biblio1 = new Biblioteca("Biblioteca Sanja", "Scarlett Mendez");
		biblio1.addLibro(libro1);
		biblio1.addLibro(libro2);
		biblio1.addLibro(libro3);
		biblio1.addLibro(libro4);
		
		System.out.println("La biblioteca llamada " + biblio1.getNombre() + " está a cargo de " + biblio1.getEncargado());
		System.out.println("Y tiene " + biblio1.getLibros().size() + " libros.");
		
        int opcion = 0; 
		while(true) {
		
			System.out.println("¿Que desea hacer?");
			System.out.println("1.- Pedir un libro");
			System.out.println("2.- Devolver libro");
			System.out.println("3.- Agregar Libro");
			System.out.println("4.- Borrar Libro");
			System.out.println("5.- Salir");
			opcion = Integer.parseInt(reader.readLine());
			
			if (opcion == 1) {
				System.out.println("¿Que libro desea pedir?");
				mostrar_libros(biblio1);
				opcion = Integer.parseInt(reader.readLine());
				biblio1.pedirLibro(opcion-1);
			}
			
			else if (opcion == 2) {
				System.out.println("¿Que libro desea devolver?");
				mostrar_libros(biblio1);
				opcion = Integer.parseInt(reader.readLine());
				biblio1.devolverLibro(opcion-1);
			}
			
			else if (opcion == 3) {
                System.out.print("Ingrese el título del libro: ");
                String nombre_libro = reader.readLine();
                System.out.print("Ingrese el autor del libro: ");
                String autor_libro = reader.readLine();
                System.out.print("Ingrese unidades disponibles del libro: ");
                int disponibles_libro = Integer.parseInt(reader.readLine());
                System.out.print("Ingrese unidades prestadas del libro: ");
                int prestados_libro = Integer.parseInt(reader.readLine());

                Libro nuevo_libro = new Libro(nombre_libro, autor_libro, disponibles_libro, prestados_libro);
                biblio1.addLibro(nuevo_libro);    
            }
			else if (opcion == 4) {
                mostrar_libros(biblio1);
                System.out.println("¿Que libro quiere borrar?");
                opcion = Integer.parseInt(reader.readLine()); 
                biblio1.borrarLibro(opcion-1);
			}
			else if (opcion == 5) {
				System.out.println("¡adiós!");
				break;
			}
		}
		
		
	}
	
	static public void mostrar_libros(Biblioteca biblio1) {
		System.out.println("Los libros son: ");
		int i = 1;
		System.out.println("|Nro.| Titulo    | Autor    | Disponibles | Prestados |");
		System.out.println("+----+-----------+----------+-------------+-----------+");
		for(Libro putalawea: biblio1.getLibros()) {
			System.out.println("| " + i + "  | " + putalawea.getTitulo() + " - " + putalawea.getAutor() + " - " + putalawea.getDisponibles() + " - " + putalawea.getPrestados() + "|");
			System.out.println("+-----------------------------------------------------+");
			i++;
		}
	}

}
