package ejercicio3;


public class Perro {

	private String nombre;
	private String color;
	private String raza;
	private int edad;
	private String sexo;
	private int peso;
	
	Perro(String nombre, String color ,String raza, int edad, String sexo, int peso){
		this.nombre = nombre;
		this.color = color;
		this.raza = raza;
		this.edad = edad;
		this.sexo = sexo;
		this.peso = peso;
		System.out.println("Se ha creado un perro: " + nombre+ " - " + color + " - " +raza + " - " + edad + " años - " +sexo+ "-"+ peso+" Kg");
	}

	public String getNombre() {
		return nombre;
	}
	
}