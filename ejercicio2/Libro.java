
public class Libro {
	//atributos
	private String titulo;
	private String autor;
	private int disponibles;
	private int prestados;
	//contructor
	Libro(String titulo, String autor, int disponibles, int prestados){
		this.titulo = titulo;
		this.autor = autor;
		this.disponibles = disponibles;
		this.prestados = prestados;
	}
    //metodos
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getDisponibles() {
		return disponibles;
	}

	public void setDisponibles(int disponibles) {
		this.disponibles = disponibles;
	}

	public int getPrestados() {
		return prestados;
	}

	public void setPrestados(int prestados) {
		this.prestados = prestados;
	}
	
}
