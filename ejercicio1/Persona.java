
public class Persona {
//Atributos
	private String nombre;
	//Contructor
	Persona(String nombre){
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}