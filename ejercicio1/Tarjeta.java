public class Tarjeta {
//Atributos
	private Persona titular;
	private double saldo;
//COntructor
	Tarjeta(Persona titular, double saldo){
		this.titular = titular;
		this.saldo = saldo;
	}
//metodos
	public Persona getTitular() {
		return titular;
	}

	public void setTitular(Persona titular) {
		this.titular = titular;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public void realizarCompra(double precio) {
		this.saldo = this.saldo - precio;
	}
	public void realizardeposito(double precio) {
		this.saldo = this.saldo +precio;
	}
}
