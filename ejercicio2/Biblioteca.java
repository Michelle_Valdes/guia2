import java.util.ArrayList;


public class Biblioteca {

	ArrayList <Libro> libros = new ArrayList<Libro>();
	//atributos
	private String nombre;
	private String encargado;
	//contructor
	Biblioteca(String nombre, String encargado){
		this.nombre = nombre;
		this.encargado = encargado;
	}
	//metodos
	public String getNombre() {
		return nombre;
	}

	public void pedirLibro(int opcion) {
		System.out.println("Se ha pedido un especimen de " + libros.get(opcion).getTitulo());
		libros.get(opcion).setDisponibles(libros.get(opcion).getDisponibles()-1);
		libros.get(opcion).setPrestados(libros.get(opcion).getPrestados()+1);
	}
	
	public void devolverLibro(int opcion) {
		System.out.println("Se ha devuelto un especimen de " + libros.get(opcion).getTitulo());
		libros.get(opcion).setDisponibles(libros.get(opcion).getDisponibles()+1);
		libros.get(opcion).setPrestados(libros.get(opcion).getPrestados()-1);
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEncargado() {
		return encargado;
	}

	public void setEncargado(String encargado) {
		this.encargado = encargado;
	}

	public void addLibro(Libro libro) {
		this.libros.add(libro);
	}

    public void borrarLibro(int indice){
        this.libros.remove(indice);
    }
	
	public ArrayList<Libro> getLibros(){
		return this.libros;
	}
}
