package ejercicio3;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		
	    int opcion = 0;
		
	 
	 System.out.print("Ingrese el nombre del perro: ");
	 String nombre_perro = reader.readLine();
	 System.out.print("Ingrese el color del perro: ");
	 String color_perro = reader.readLine();
     System.out.print("Ingrese la raza del animal: ");
     String raza_perro = reader.readLine();
     System.out.print("Ingrese la edad de la mascota: ");
     int edad_perro = Integer.parseInt(reader.readLine());
     System.out.print("Ingrese el sexo(hembra/macho) del animal: ");
	 String sexo_perro = reader.readLine();
     System.out.print("Ingrese el peso del perro en Kg: ");
     int peso_perro = Integer.parseInt(reader.readLine());

     
     Perro perro_temporal = new Perro(nombre_perro, color_perro, raza_perro, edad_perro, sexo_perro, peso_perro);
     
		
     while(true) {
			
			System.out.println("¿Que desea que haga el perro?");
			System.out.println("1.- comer");
			System.out.println("2.- jugar");
			System.out.println("3.- dormir");
			System.out.println("4.- Salir");
			opcion = Integer.parseInt(reader.readLine());

			if (opcion == 1) {
				System.out.println("El perro llamado " + perro_temporal.getNombre()+ " está comiendo");
			}
			else if (opcion == 2) {
				System.out.println("El perro llamado " + perro_temporal.getNombre()+ " está jugando");
			}
			else if(opcion == 3)
				System.out.println("El perro llamado " + perro_temporal.getNombre()+ " está durmiendo");
			else if (opcion == 4) {
				System.out.println("¡Hasta luego!");
				break;
			}
		
		
	}	
}
}
